#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtChart

import logging
import json
import argparse
import sys
import os
import tensorflow as tf
import numpy as np

from main_window import Ui_main_window
import config
import dice_rcc
from builtins import RuntimeError

logger = logging.getLogger (__name__)

class DNNPosition:
    def __init__ (self, learning_rate, model_dir):
        self.estimator = None
        self.learning_rate = learning_rate
        self.model_dir = model_dir

    def setLearningRate (self, learning_rate):
        if self.learning_rate != learning_rate:
            self.learning_rate = learning_rate
            self.estimator = None
        
    def setModelDir (self, model_dir):
        if self.model_dir != model_dir:
            self.model_dir = model_dir
            self.estimator = None
                
    def createEstimator (self):
        self.estimator = tf.estimator.Estimator(
                        model_fn=self.ModelFN,
                        params={
                            "layers": [4096, 1024, 256, 32, 4],
                            #"layers": [2048, 512, 256, 128, 64, 32, 4],
                            "learning_rate": self.learning_rate,
                            "optimizer": tf.train.AdamOptimizer
                        },
                        model_dir=self.model_dir)

    @staticmethod
    def ModelFN (features, labels, mode, params):
      """Model function."""
      # Input Layer
      layers = params.get ("layers", [32, 4])
    
      layer = tf.reshape (features["images_small"], [-1, 92 * 69])
    
      for lsize in layers:
          layer = tf.layers.dense (inputs=layer, units=lsize, activation=tf.nn.relu)
    
      predictions = tf.squeeze (layer)
    
      if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec (mode=mode, predictions={"position": predictions})
    
      # Calculate Loss (for both TRAIN and EVAL modes)
      average_loss = tf.losses.mean_squared_error (labels, predictions)
    
      # Pre-made estimators use the total_loss instead of the average,
      # so report total_loss for compatibility.
      batch_size = tf.shape (labels)[0]
      total_loss = tf.to_float (batch_size) * average_loss
    
      if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = params.get ("optimizer", tf.train.AdamOptimizer)
        optimizer = optimizer (params.get ("learning_rate", None))
        train_op = optimizer.minimize (loss=average_loss, global_step=tf.train.get_global_step ())
    
        return tf.estimator.EstimatorSpec(mode=mode, loss=total_loss, train_op=train_op)
    
      # In evaluation mode we will calculate evaluation metrics.
      assert mode == tf.estimator.ModeKeys.EVAL
    
      # Calculate root mean squared error
      rmse = tf.metrics.root_mean_squared_error (labels, predictions)
    
      # Add the rmse to the collection of evaluation metrics.
      eval_metrics = {"rmse": rmse}
    
      return tf.estimator.EstimatorSpec (
          mode=mode,
          # Report sum of error for compatibility with pre-made estimators
          loss=total_loss,
          eval_metric_ops=eval_metrics)
    
    def train (self, train_input_function, eval_input_function, hooks=None, iterations=1):
        r = { 'global_step': 0,
              'loss': 0.0,
              'rmse': 0.0 }
        
        if self.estimator == None:
            self.createEstimator ()
        
        try:
            for i in range (iterations):
                self.estimator.train (input_fn=train_input_function, hooks=hooks, steps=30)
                r = self.estimator.evaluate (input_fn=eval_input_function, hooks=hooks, steps=1)
                print ("steps: {} loss: {} rmse: {}".format (r['global_step'], r['loss'], r['rmse']))
        except Exception as e:
            return (False, e)

        return (True, r)

    def eval (self, input_function, hooks=None):
        if self.estimator == None:
            self.createEstimator ()
        
        pos_x1, pos_y1, pos_x2, pos_y2 = [ v['position'] for v in self.estimator.predict (input_fn=input_function, predict_keys="position", hooks=hooks) ]

        return (pos_x1, pos_y1, pos_x2, pos_y2)
        #return self.estimator.evaluate (input_fn=input_function, steps=1)

class TrainThread (QtCore.QThread):
    iterationSignal = QtCore.pyqtSignal (int, float)
    finishedSignal = QtCore.pyqtSignal ()
    
    def __init__ (self, trainer):
        QtCore.QThread.__init__ (self)
        self.trainer = trainer
        self.abort = False
        self.error = None
    
    def run (self):
        while not self.abort:
            r = self.trainer ()
            if r[0]:
                self.iterationSignal.emit (r[1]['global_step'], r[1]['rmse'])
            else:
                self.error = r[1]
                break

        self.finishedSignal.emit ()
         
    def abortTraining (self):
        self.abort = True

class SnapError (RuntimeError):
    def __init__ (self, msg=None, qt_process_error = None):
        RuntimeError.__init__ (self, msg)
        self.qt_process_error = qt_process_error
        
    def __str__ (self):
        msg = RuntimeError.__str__ (self)
        if self.qt_process_error == QtCore.QProcess.FailedToStart:
            msg += "\n" + QtCore.QCoreApplication.translate ("error_msg",
                                                             "Invocation failed. Check the command in the preferences.")
        elif self.qt_process_error == QtCore.QProcess.Crashed:
            msg += "\n" + QtCore.QCoreApplication.translate ("error_msg",
                                                             "Process crashed. Check the command in the preferences.")
        elif self.qt_process_error == QtCore.QProcess.WriteError:
            msg += "\n" + QtCore.QCoreApplication.translate ("error_msg",
                                                             "Write error")
        elif self.qt_process_error == QtCore.QProcess.ReadError:
            msg += "\n" + QtCore.QCoreApplication.translate ("error_msg",
                                                             "Read error")
            
        return msg
        
class SnapThread (QtCore.QThread):
    imageSnappedSignal = QtCore.pyqtSignal ("QString")

    def __init__ (self, fname):
        QtCore.QThread.__init__ (self)
        self.fname = fname
        self.error = None
    
    def run (self):
        try:
            cmd = config.cfg.capture_command
            p = QtCore.QProcess ()
            #print ("start", repr (cmd))
            p.start (cmd)

            if not p.waitForStarted(-1):
                raise SnapError (QtCore.QCoreApplication.translate ("error_msg",
                                                                    "Image snap process failed"),
                                 p.error ())
            
            f = open (self.fname, "wb")
            size = 0
            while p.state () != QtCore.QProcess.NotRunning:
                if p.waitForReadyRead (-1):
                    b = p.read (1024*1024)
                
                    if len (b) > 0:
                        f.write (b)
                        size += len (b)
    
            f.close ()
            if size == 0:
                raise SnapError (QtCore.QCoreApplication.translate ("error_msg",
                                                                    "Empty image snapped"))

            self.imageSnappedSignal.emit (self.fname)
        except Exception as e:
            self.error = e
            self.imageSnappedSignal.emit ("")

class ImageEntry:
    def __init__ (self, file_name, value = None, pos_x = None, pos_y = None, pos_x2 = None, pos_y2 = None):
        self.file_name = file_name
        if pos_x2 == None and pos_x != None:
            self.pos_x1 = pos_x - 225
            self.pos_x2 = pos_x + 225
        else:
            self.pos_x1 = pos_x
            self.pos_x2 = pos_x2
            
        if pos_y2 == None and pos_y != None:
            self.pos_y1 = pos_y - 225
            self.pos_y2 = pos_y + 225
        else:
            self.pos_y1 = pos_y
            self.pos_y2 = pos_y2
            
        self.value = value
        self.cached_data = {}
        
    def hasPos (self):
        return self.pos_x1 != None and self.pos_y1 != None and self.pos_x2 != None and self.pos_y2 != None
    
    def hasValue (self):
        return self.value != None
    
    def hasCachedData (self, name):
        return name in self.cached_data
    
    def getCachedData (self, name):
        return self.cached_data[name]
    
    def setCachedData (self, name, data):
        self.cached_data[name] = data

class ImagesModel (QtGui.QStandardItemModel):
    
    IMAGE_NAME, POS_X1, POS_Y1, POS_X2, POS_Y2, VALUE, COLUMNS = range (7)
    
    def __init__ (self, main_window):
        QtGui.QStandardItemModel.__init__ (self, 0, 6)
        self.setDataList([], "")
        self.main_window = main_window
        
    def numberOfTrainableImages (self):
        n = 0
        for d in self.data_list:
            if d.hasPos ():
                n += 1
                
        return n
        
    def numberOfImages (self):
        return len (self.data_list)
    
    def changeBaseDir (self, base_dir):
        if len (self.data_list) == 0:
            self.base_dir = base_dir
        else:
            print ("Change of base directory not supported when images are existing.")
            
        return self.base_dir
        
    def setDataList (self, data, base_dir):
        self.base_dir = base_dir
        self.beginResetModel ()
        self.data_list = data
        self.endResetModel ()
        self.setRowCount (len (data))
        self.dataChanged.emit (self.index (0,0), self.index (len (data)-1, ImagesModel.COLUMNS-1))
        
    def addNewImage (self, image_entry):
        pos = len (self.data_list)
        self.beginInsertRows (QtCore.QModelIndex (), pos, pos)
        self.data_list.append (image_entry)
        self.endInsertRows ()
        self.setRowCount (len (self.data_list))
        self.dataChanged.emit (self.index (pos,0), self.index (pos, ImagesModel.COLUMNS-1))
        return pos

    def removeImage (self, row):
        if row < len (self.data_list):
            self.beginRemoveRows (QtCore.QModelIndex (), row, row)
            del self.data_list[row]
            self.endRemoveRows ()

    def changeDicePosition (self, row, x1, y1, x2, y2):
        if row < len (self.data_list):
            self.data_list[row].pos_x1 = x1
            self.data_list[row].pos_y1 = y1
            self.data_list[row].pos_x2 = x2
            self.data_list[row].pos_y2 = y2
            self.dataChanged.emit (self.index (row, ImagesModel.POS_X1), self.index (row, ImagesModel.POS_Y2))

    def changeDiceValue (self, row, value):
        if row < len (self.data_list):
            self.data_list[row].value = value
            self.dataChanged.emit (self.index (row, ImagesModel.VALUE), self.index (row, ImagesModel.VALUE))

    def loadSmallImage (self, fname, *label):
        #print ("###### fname:", fname)
        label = [ tf.cast (l, dtype=tf.float32) for l in label ]

        image_string = tf.read_file (fname)
        image = tf.image.decode_jpeg (image_string)
        image = tf.image.convert_image_dtype (image, dtype=tf.float32)
        image = tf.image.resize_images (image, [92, 69])
        image = tf.image.rgb_to_grayscale (image)
        image = tf.reshape (image, (6348, ))
        return ( {"images_small": image }, *label )
    
    def getPosData (self, rows, batches, repeat, use_positions = True):
        filenames = [ os.path.join (self.base_dir, self.data_list[row].file_name) for row in rows ]
        #numbers = [ self.data_list[row].value for row in rows ]
        pos = []
        
        if use_positions:
            pos = [ (self.data_list[row].pos_x1, self.data_list[row].pos_y1,
                     self.data_list[row].pos_x2, self.data_list[row].pos_y2) for row in rows ]
        else:
            pos = [ (0.0, 0.0, 0.0, 0.0) for row in rows ]
    
        dataset = tf.data.Dataset.from_tensor_slices ( (filenames, pos) )
            
        dataset = dataset.map (self.loadSmallImage)
        dataset = dataset.shuffle (buffer_size=1)
        dataset = dataset.batch (batches)
        dataset = dataset.repeat (repeat)
        
        iterator = dataset.make_one_shot_iterator()
    
        # `features` is a dictionary in which each value is a batch of values for
        # that feature; `labels` is a batch of labels.
        features, labels = iterator.get_next()
        return features, labels

    def getTrainingPosData (self):
        return self.getPosData (range (len (self.data_list)), 19, 5)
    
    def getEvalPosData (self):
        return self.getPosData (range (len (self.data_list)), 22, 1)
    
    def getPredictPosData (self, row):
        return self.getPosData ([ row ], 1, 1, False)
    
    def columnCount (self, parent_index):
        if parent_index == QtCore.QModelIndex ():
            return ImagesModel.COLUMNS
        
        return 0
    
    def headerData (self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal:
            if role == QtCore.Qt.DisplayRole:
                if section == ImagesModel.IMAGE_NAME:
                    return QtCore.QCoreApplication.translate ("image_list", "Image")
                
                elif section == ImagesModel.POS_X1:
                    return QtCore.QCoreApplication.translate ("image_list", "X1")
                
                elif section == ImagesModel.POS_Y1:
                    return QtCore.QCoreApplication.translate ("image_list", "Y1")
                
                elif section == ImagesModel.POS_X2:
                    return QtCore.QCoreApplication.translate ("image_list", "X2")
                
                elif section == ImagesModel.POS_Y2:
                    return QtCore.QCoreApplication.translate ("image_list", "Y2")
                
                elif section == ImagesModel.VALUE:
                    return QtCore.QCoreApplication.translate ("image_list", "Value")
                
        return None

    def data (self, index, role):
        if index.row () < len (self.data_list):
            row = index.row ()
            col = index.column ()
            if role == QtCore.Qt.DisplayRole:
                if col == ImagesModel.IMAGE_NAME:
                    return os.path.basename (self.data_list[row].file_name)
                
                elif col == ImagesModel.POS_X1:
                    if self.data_list[row].hasPos ():
                        return "{}".format (self.data_list[row].pos_x1)
                    else:
                        return "---"
                    
                elif col == ImagesModel.POS_Y1:
                    if self.data_list[row].hasPos ():
                        return "{}".format (self.data_list[row].pos_y1)
                    else:
                        return "---"
                    
                elif col == ImagesModel.POS_X2:
                    if self.data_list[row].hasPos ():
                        return "{}".format (self.data_list[row].pos_x2)
                    else:
                        return "---"
                    
                elif col == ImagesModel.POS_Y2:
                    if self.data_list[row].hasPos ():
                        return "{}".format (self.data_list[row].pos_y2)
                    else:
                        return "---"
                    
                elif col == ImagesModel.VALUE:
                    if self.data_list[row].hasValue ():
                        return "{}".format (self.data_list[row].value)
                    else:
                        return "---"
                
            elif role == QtCore.Qt.TextAlignmentRole:
                if col == ImagesModel.IMAGE_NAME:
                    return QtCore.Qt.AlignLeft
                else:
                    return QtCore.Qt.AlignRight
        
        return None
    
class DiceItem (QtWidgets.QGraphicsRectItem):
    def __init__ (self, image_widget):
        QtWidgets.QGraphicsRectItem.__init__ (self)
        self.image_widget = image_widget
        pen = QtGui.QPen (QtGui.QColor (100, 150, 100))
        pen.setWidthF (2.5)
        self.setPen (pen)
        brush = QtGui.QBrush (QtGui.QColor (100, 150, 100, 50))
        self.setBrush (brush)
        
        self.resize_active = False

    def paint (self, painter, option, widget):
        painter.setPen (self.pen ())
        painter.setBrush (self.brush ())
        r = self.rect ()
        painter.drawRect (r)

        brush = QtGui.QBrush (QtGui.QColor (150, 180, 150))        
        painter.setBrush (brush)
        r2 = QtCore.QRectF (r.right ()-12, r.bottom ()-12, 12, 12)
        painter.drawRect (r2)

    def mouseMoveEvent (self, event):
        rect = self.rect ()
        
        if self.resize_active:
            rect.translate (self.pos ())
            p = event.pos () + self.pos ()
            x = p.x ()
            if rect.left () + 30 > x:
                x = rect.left () + 30

            y = p.y ()
            if rect.top () + 30 > y:
                y = rect.top () + 30
            
            rect.setBottomRight (QtCore.QPointF (x, y))
            self.setPos (0, 0)
            self.setRect (rect)
            self.image_widget.diceMoved (rect)
            return
        
        QtWidgets.QGraphicsRectItem.mouseMoveEvent (self, event)
        rect.translate (self.pos ())
        #print ("rect:", rect)
        self.image_widget.diceMoved (rect)
        
    def mousePressEvent(self, event):
        if event.button () == QtCore.Qt.LeftButton:
            p =  self.rect ().bottomRight () - event.pos ()
            if p.x () >= 0 and p.x () < 12 and p.y () >= 0 and p.y () < 12:
                #print ("pos:", event.pos ())
                self.resize_active = True
                
        QtWidgets.QGraphicsRectItem.mousePressEvent(self, event)
        
    def mouseReleaseEvent(self, event):
        self.resize_active = False
        QtWidgets.QGraphicsRectItem.mouseReleaseEvent(self, event)


class ImageWidget (QtWidgets.QGraphicsView):
    MAX_SCALING = 30.0
    
    rectSelected = QtCore.pyqtSignal (QtCore.QRectF)

    def __init__ (self, parent):
        QtWidgets.QGraphicsView.__init__ (self, parent)
        self.setScene (QtWidgets.QGraphicsScene (self))
        self.setRenderHints (QtGui.QPainter.Antialiasing | QtGui.QPainter.SmoothPixmapTransform)

        self.pixmap = QtWidgets.QGraphicsPixmapItem ()
        self.dice_position = DiceItem (self)
        self.dice_position.setVisible (False)
        
        self.scene ().addItem (self.pixmap)
        self.scene ().addItem (self.dice_position)

        self.min_scaling = 1.0
        self.reset_scaling_on_next_paint = False
        self.start_move = None
        self.has_pixmap = False
        self.in_dice_move = False
        
    def diceMoved (self, rect):
        self.in_dice_move = True
        self.rectSelected.emit (rect)
        self.in_dice_move = False

    def setImage (self, pixmap):
        reset_scaling = not self.has_pixmap
        self.has_pixmap = isinstance (pixmap, QtGui.QPixmap) and pixmap.width () > 0 and pixmap.height () > 0

        if self.has_pixmap:
            self.pixmap.setPixmap (pixmap)
            self.pixmap.setVisible (True)
            fx = self.width () / pixmap.width ()
            fy = self.height () / pixmap.height ()
            self.min_scaling = min ((fx, fy))
            if reset_scaling:
                self.setScaling (self.min_scaling)
            else:
                self.changeScaling (1.0) 
        else:
            self.pixmap.setPixmap (QtGui.QPixmap ())
            self.pixmap.setVisible (False)
            self.min_scaling = 1.0
            self.scale (1.0, 1.0)
            
    def setDicePosition (self, pos):
        if self.in_dice_move:
            return
        
        if pos != None:
            self.dice_position.setPos (0, 0)
            self.dice_position.setRect (pos)
            self.dice_position.setVisible (True)
            self.dice_position.setFlag (QtWidgets.QGraphicsItem.ItemIsMovable, True)
        else:
            self.dice_position.setVisible (False)

    def mousePressEvent(self, event):
        if event.button () == QtCore.Qt.LeftButton and event.modifiers () == QtCore.Qt.ControlModifier:
            p = event.pos ()
            p = self.mapToScene (p)
            rect = QtCore.QRectF (p, p + QtCore.QPointF (30, 30))
            self.setDicePosition (rect)
            self.diceMoved (rect)
            
        QtWidgets.QGraphicsView.mousePressEvent (self, event)
    
    def wheelEvent (self, event):
        if event.modifiers () == QtCore.Qt.ControlModifier:
            up_down = event.angleDelta ().y ()
            left_right = event.angleDelta ().x ()
              
            if up_down != 0:
                if up_down > 0:
                    self.changeScaling (1.1, QtCore.QPoint (event.x (), event.y ()))
 
                elif up_down < 0:
                    self.changeScaling (1 / 1.1, QtCore.QPoint (event.x (), event.y ()))
                 
                return
             
        return QtWidgets.QGraphicsView.wheelEvent (self, event)
        
    def changeScaling (self, rel_factor, keep_point = None):
        t_old = self.transform ()
        old_sf = 1.0 / t_old.m33 ()
        
        abs_sf = old_sf * rel_factor
        
        if abs_sf < self.min_scaling:
            abs_sf = self.min_scaling
            rel_factor = abs_sf / old_sf
        elif abs_sf > ImageWidget.MAX_SCALING:
            abs_sf = ImageWidget.MAX_SCALING
            rel_factor = abs_sf / old_sf
        
        t = QtGui.QTransform (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0, 1.0/rel_factor)
        
        #print ("rel {} * old {} = {}".format (rel_factor, old_sf, rel_factor * old_sf))
        self.setTransform (t, True)

        # Folgendes funktioniert leider noch nicht. Es soll eigentlich die angegebene Position erhalten bleiben
        # (Mausposition beim Zoom mit dem Mausrad)
        if keep_point != None:
            dx = keep_point.x () * (abs_sf - 1) - t_old.m31 ()
            dy = keep_point.y () * (abs_sf - 1) - t_old.m32 ()
            #print ("d: {}, {} (old: {}, {})".format (dx, dy, t_old.m31 (), t_old.m32 ()))

            #self.scrollContentsBy (int (dx), int (dy))
            
    def setScaling (self, abs_factor):
        if abs_factor < self.min_scaling:
            abs_factor = self.min_scaling
        elif abs_factor > ImageWidget.MAX_SCALING:
            abs_factor = ImageWidget.MAX_SCALING

        t = QtGui.QTransform (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0, 1.0 / abs_factor)
        
        self.setTransform (t, False)

class SessionRunHook (tf.train.SessionRunHook):
    def __init__ (self, main_window):
        self.main_window = main_window
        
    def after_create_session (self, session, coord):
        #print ("create session:", repr (session))
        self.main_window.setTFSession (session)
        
    def end (self, session):
        #print ("end session:", repr (session))
        self.main_window.setTFSession (None)

class FileNameWithNumber:
    def __init__ (self, directory, file_name, number, extension):
        self.directory = directory
        self.file_name = file_name
        self.number = number
        self.extension = extension
        
    def __str__ (self):
        return os.path.join (self.directory, "{}{}{}".format (self.file_name, self.number, self.extension))
    
    def setDirectory (self, directory):
        self.directory = directory
        
    def setNumber (self, number):
        self.number = number
        
    def increase (self):
        self.number += 1

    def increaseToNextNonexisting (self):
        while os.path.exists (str (self)):
            self.number += 1
        
class MainWindow (QtWidgets.QMainWindow):
    ui = Ui_main_window ()
    
    def __init__ (self):
        self.img = None
        QtWidgets.QMainWindow.__init__ (self)
        self.ui = Ui_main_window ()
        self.ui.setupUi (self)
        
        self.grid_layout_chart = QtWidgets.QGridLayout (self.ui.chart_w)
        self.grid_layout_chart.setContentsMargins (0, 0, 0, 0)
        self.grid_layout_chart.setObjectName ("grid_layout_chart")

        self.chart_view_w = QtChart.QChartView (self.ui.chart_w)
        self.grid_layout_chart.addWidget (self.chart_view_w, 0, 0, 1, 1)
        self.chart = self.chart_view_w.chart ()
        
        self.ui.dice_value_sb.setMinimum (0)
        self.ui.dice_value_sb.setMaximum (20)
        self.ui.snap_image_btn.clicked.connect (self.snapImageClicked)
        self.ui.analyze_image_btn.clicked.connect (self.analyzeImageClicked)
        self.ui.change_data_btn.clicked.connect (self.changeDataClicked)
        self.ui.train_btn.clicked.connect (self.trainClicked)
        self.ui.actionLoad.triggered.connect (self.loadFile)
        self.ui.actionSave.triggered.connect (self.saveFile)
        self.ui.actionQuit.triggered.connect (self.quitProgram)
        self.ui.actionPreferences.triggered.connect (self.editPreferences)
        self.ui.actionSnapImage.triggered.connect (self.snapImageClicked)
        self.ui.actionImportImage.triggered.connect (self.importImage)
        self.ui.actionAnalyzeImage.triggered.connect (self.analyzeImageClicked)
        self.ui.actionDeleteImage.triggered.connect (self.deleteImage)
        self.ui.actionStartTraining.triggered.connect (self.startTraining)
        self.ui.actionStopTraining.triggered.connect (self.stopTraining)
        self.ui.actionResetNetwork.triggered.connect (self.resetNetwork)
        self.image_model = ImagesModel (self)
        self.ui.images_tbl.setModel (self.image_model)
        self.ui.images_tbl.header ().setSectionResizeMode (QtWidgets.QHeaderView.ResizeToContents)
        self.ui.images_tbl.selectionModel ().selectionChanged.connect (self.imageSelected)
        
        self.grid_layout_image = QtWidgets.QGridLayout (self.ui.image_container_w)
        self.grid_layout_image.setContentsMargins (0, 0, 0, 0)
        self.image_w = ImageWidget (self.ui.image_container_w)
        self.grid_layout_image.addWidget(self.image_w, 0, 0, 1, 1)
        
        self.image_w.rectSelected.connect (self.diceRectSelected)

        self.file_valid = False
        self.file_name = config.cfg.base_dir
        self.image_base_dir = os.path.join (config.cfg.base_dir, "Images")
        self.image_file_name = FileNameWithNumber (self.image_base_dir, "image_", 1, ".jpg")
        self.position_model_dir = os.path.join (config.cfg.base_dir, "PositionModel")

        self.dnn_position = DNNPosition (config.cfg.learning_rate, self.position_model_dir)
        self.tf_session = None
        
        self.tf_hooks = [ SessionRunHook (self) ]
        self.train_thread = None
        self.snap_thread = None
        
        self.ignore_signals = False
        
        self.rmse_series = []
        
        self.actual_translator = None
        self.updateSensitivities ()
        
    def showErrorDialog (self, title, message):
        QtWidgets.QMessageBox.warning (self, title, message,
                                       QtWidgets.QMessageBox.Ok,
                                       QtWidgets.QMessageBox.Ok)

    def retranslate (self):
        if self.actual_translator != None:
            QtWidgets.qApp.removeTranslator (self.actual_translator)
            self.actual_translator = None
            
        locale = config.cfg.getLocale ()
        QtCore.QLocale.setDefault (locale)
    
        translator = QtCore.QTranslator ()
        if translator.load (locale, "dice", "_", ":/translations"): 
            app.installTranslator (translator)
            self.actual_translator = translator

    def changeEvent (self, event):
        if event.type () == QtCore.QEvent.LanguageChange:
            self.ui.retranslateUi (self)

        QtWidgets.QMainWindow.changeEvent (self, event)

    def setTFSession (self, session):
        self.tf_session = session

    def importImage (self):
        print ("ToDo: implement importImage")
        self.updateSensitivities ()
    
    def snapImageClicked (self):
        if self.snap_thread != None:
            return
        
        self.ui.snap_image_btn.setEnabled (False)
        
        if not os.path.exists (self.image_file_name.directory):
            os.mkdir (self.image_file_name.directory)
            
        fname = str (self.image_file_name)
        self.snap_thread = SnapThread (fname)
        self.snap_thread.imageSnappedSignal.connect (self.imageSnapped)
        self.snap_thread.start ()
        self.image_file_name.increase ()

    def imageSnapped (self, fname):
        if fname != None and fname != "":
            pos = self.image_model.addNewImage (ImageEntry (os.path.basename (fname)))
            self.selectImage (pos)
        elif self.snap_thread.error != None:
            self.showErrorDialog (QtCore.QCoreApplication.translate ("error_title",
                                                                     "Snap image failed"),
                                  str (self.snap_thread.error))
            
        self.snap_thread = None
        self.ui.snap_image_btn.setEnabled (True)
        self.updateSensitivities ()
        
    def analyzeImageClicked (self):
        if self.train_thread != None:
            self.ui.statusbar.showMessage \
                (QtCore.QCoreApplication.translate ("status_bar", "Training active"),
                 10000)
            return

        sel_list = self.ui.images_tbl.selectionModel ().selection ().indexes ()
        row = None
        
        for s in sel_list:
            if s.row () < len (self.image_model.data_list):
                row = s.row ()
                break
        
        if row != None:
            self.dnn_position.setModelDir (self.position_model_dir)
            self.dnn_position.setLearningRate (config.cfg.learning_rate)
            try:
                r = self.dnn_position.eval (lambda: self.image_model.getPredictPosData (row), hooks=self.tf_hooks)
                self.diceRectSelected (QtCore.QRectF (QtCore.QPointF (r[0], r[1]), QtCore.QPointF (r[2], r[3])))
                print ("Result for row {}: {}".format (row, r))
            except ValueError as e:
                self.ui.statusbar.showMessage \
                    (QtCore.QCoreApplication.translate ("status_bar", "Analyse failed: {}")
                     .format (e),
                     10000)
        
    def deleteImage (self):
        if self.train_thread != None:
            self.ui.statusbar.showMessage \
                (QtCore.QCoreApplication.translate ("status_bar", "Training active"),
                 10000)
            return

        sel_list = self.ui.images_tbl.selectionModel ().selection ().indexes ()
        row = None
        
        for s in sel_list:
            if s.row () < len (self.image_model.data_list):
                row = s.row ()
                break
        
        if row != None:
            self.image_model.removeImage (row)
        
    def changeDataClicked (self):
        print ("Change Data Clicked")
        sel_list = self.ui.images_tbl.selectionModel ().selection ().indexes ()
        row = None
        
        for s in sel_list:
            if s.row () < len (self.image_model.data_list):
                row = s.row ()
                break
        
        if row != None:
            self.image_model.changeDicePosition (row, self.ui.dice_position_x1_sb.value (),
                                                 self.ui.dice_position_y1_sb.value (),
                                                 self.ui.dice_position_x2_sb.value (),
                                                 self.ui.dice_position_y2_sb.value ())
            self.image_model.changeDiceValue (row, self.ui.dice_value_sb.value ())
            self.updateSensitivities ()

    def resetNetwork (self):
        print ("ToDo: implement resetNetwork")
        pass
    
    def updateSensitivities (self):
        train_running = self.train_thread != None
        num_images = self.image_model.numberOfImages ()
        num_train_images = self.image_model.numberOfTrainableImages ()
        self.file_valid
        self.ui.analyze_image_btn.setEnabled (not train_running and num_images > 0 and self.file_valid)
        self.ui.actionAnalyzeImage.setEnabled (not train_running and num_images > 0 and self.file_valid)
        self.ui.actionDeleteImage.setEnabled (not train_running and num_images > 0 and self.file_valid)
        self.ui.actionStartTraining.setEnabled (not train_running and num_train_images > 0 and self.file_valid)
        self.ui.actionStopTraining.setEnabled (train_running)

        self.ignore_signals = True
        self.ui.train_btn.setChecked (train_running)
        self.ui.train_btn.setEnabled (num_train_images > 0 and self.file_valid)
        self.ignore_signals = False
        
        self.ui.snap_image_btn.setEnabled (self.file_valid)
    
    def startTraining (self):
        if self.train_thread != None:
            self.ui.statusbar.showMessage \
                (QtCore.QCoreApplication.translate ("status_bar", "Training is still running"),
                 10000)
            self.updateSensitivities ()
            return
        
        if self.image_model.rowCount () == 0:
            self.ui.statusbar.showMessage \
                (QtCore.QCoreApplication.translate ("status_bar", "No training data exists"),
                 10000)
            self.updateSensitivities ()
            return

        self.dnn_position.setModelDir (self.position_model_dir)
        self.dnn_position.setLearningRate (config.cfg.learning_rate)
        self.train_thread = TrainThread (lambda: self.dnn_position.train (self.image_model.getTrainingPosData,
                                                                          self.image_model.getEvalPosData, hooks=self.tf_hooks))
        self.train_thread.iterationSignal.connect (self.trainingInfo)
        self.train_thread.finishedSignal.connect (self.trainingFinished)
        self.train_thread.start ()
        self.ui.statusbar.showMessage \
            (QtCore.QCoreApplication.translate ("status_bar", "Training started"), 10000)
            
        self.updateSensitivities ()
    
    def stopTraining (self):

        if self.train_thread == None:
            self.ui.statusbar.showMessage \
                (QtCore.QCoreApplication.translate ("status_bar", "No training started"), 10000)
            self.updateSensitivities ()
            return

        self.train_thread.abortTraining ()
        self.ui.statusbar.showMessage \
            (QtCore.QCoreApplication.translate ("status_bar", "Abort training signaled"), 10000)

        self.updateSensitivities ()
    
    def trainClicked (self, checked):
        if self.ignore_signals:
            return
        
        if checked:
            self.startTraining ()
        else:
            self.stopTraining ()
            
    def trainingInfo (self, iterations, rmse):
        self.ui.statusbar.showMessage \
            (QtCore.QCoreApplication.translate ("status_bar",
                                                "Iteration {}, root mean squared error: {}")
                                                .format (iterations, rmse),
             10000)
        self.rmse_series.append ([iterations, rmse])
        self.updateChart ()
        
    def trainingFinished (self):
        if self.train_thread != None and self.train_thread.error != None:
            self.showErrorDialog (QtCore.QCoreApplication.translate ("error_title",
                                                                     "Training error"),
                                  str (self.train_thread.error))

        self.train_thread = None
        self.updateSensitivities ()
        
    def updateChart (self):
        l = []
        for d in self.rmse_series:
            l.append (QtCore.QPointF (d[0], d[1]))

        curve = QtChart.QLineSeries ()
        pen = curve.pen ()
        pen.setWidthF (.1)
        curve.setPen (pen)
        #curve.setUseOpenGL (True)
        curve.replace (l)

        self.chart.removeAllSeries ()
        self.chart.addSeries (curve)
        self.chart.createDefaultAxes ()
        #self.chart_x_axis = QValueAxis ()

    
    def diceRectSelected (self, rect):
        #print ("point:", point)
        x1 = rect.left ()
        x2 = rect.right ()
        y1 = rect.top ()
        y2 = rect.bottom ()
        
        if x1 > x2:
            t = x1
            x1 = x2
            x2 = t
            
        if y1 > y2:
            t = y1
            y1 = y2
            y2 = t
            
        if x2 - x1 < 200:
            d = (200 - (x2 - x1)) / 2
            x1 -= d
            x2 += d
            
        if y2 - y1 < 200:
            d = (200 - (y2 - y1)) / 2
            y1 -= d
            y2 += d
            
        self.ui.dice_position_x1_sb.setValue (x1)
        self.ui.dice_position_y1_sb.setValue (y1)
        self.ui.dice_position_x2_sb.setValue (x2)
        self.ui.dice_position_y2_sb.setValue (y2)
        self.image_w.setDicePosition (QtCore.QRectF (QtCore.QPointF (x1, y1), QtCore.QPointF (x2, y2)))
        
    def updateImages (self):
        pass
    
    def updateImage (self):
        self.image_w.setImage (self.img)
        
    def showImage (self, row):
        if row < len (self.image_model.data_list):
            if self.image_model.data_list[row].hasCachedData ("display_image"):
                self.img = self.image_model.data_list[row].getCachedData ("display_image")
            else:
                fn = os.path.join (self.image_base_dir, self.image_model.data_list[row].file_name)
                self.img = QtGui.QPixmap ()
                self.img.load (fn)
                if self.img.width () < self.img.height ():
                    t = QtGui.QTransform (0, -1, 1, 0, 0, 0)
                    img2 = self.img.transformed (t)
                    self.img = img2
                    
                self.image_model.data_list[row].setCachedData ("display_image", self.img)
                
            #print ("Load:", fn)
            self.ui.dice_position_x1_sb.setMaximum (self.img.width ())
            self.ui.dice_position_y1_sb.setMaximum (self.img.height ())
            self.ui.dice_position_x2_sb.setMaximum (self.img.width ())
            self.ui.dice_position_y2_sb.setMaximum (self.img.height ())
            
            self.updateImage ()
            if self.image_model.data_list[row].hasValue ():
                self.ui.dice_value_sb.setValue (self.image_model.data_list[row].value)

            if self.image_model.data_list[row].hasPos ():
                img_entry = self.image_model.data_list[row]
                self.image_w.setDicePosition (QtCore.QRectF (QtCore.QPointF (img_entry.pos_x1, img_entry.pos_y1),
                                                             QtCore.QPointF (img_entry.pos_x2, img_entry.pos_y2)))
                self.ui.dice_position_x1_sb.setValue (self.image_model.data_list[row].pos_x1)
                self.ui.dice_position_y1_sb.setValue (self.image_model.data_list[row].pos_y1)
                self.ui.dice_position_x2_sb.setValue (self.image_model.data_list[row].pos_x2)
                self.ui.dice_position_y2_sb.setValue (self.image_model.data_list[row].pos_y2)
            else:
                self.image_w.setDicePosition (None)

    def imageSelected (self, selected):
        #print ("img selected")
        sel_list = selected.indexes ()
        for sel in sel_list:
            row = sel.row ()
            if row < len (self.image_model.data_list):
                self.showImage (row)
                return
    
    def selectImage (self, row):
        sel_model = self.ui.images_tbl.selectionModel ()
        index1 = self.image_model.index (row, 0)
        index2 = self.image_model.index (row, ImagesModel.COLUMNS-1)
        sel_model.select (QtCore.QItemSelection (index1, index2),
                          QtCore.QItemSelectionModel.ClearAndSelect | QtCore.QItemSelectionModel.Current)
        
    def editPreferences (self):
        config.cfg.editConfig ()

    def getFileTypes (self):
        file_types = [(QtCore.QCoreApplication.translate ("file_types", "All files"), "*"),
                      (QtCore.QCoreApplication.translate ("file_types", "JSON files"), "*.json")]
        ft_list = ";;".join (["{} ({})".format (ft[0], ft[1]) for ft in file_types])
        ft_default = "{} ({})".format (file_types[1][0], file_types[1][1])
        return (ft_list, ft_default)

    def loadFile (self):
        options = QtWidgets.QFileDialog.Options ()
        ft_list, ft_default = self.getFileTypes ()
        file, used_filter = QtWidgets.QFileDialog.getOpenFileName \
            (self, QtCore.QCoreApplication.translate ("load_file", "Load File"),
             config.cfg.base_dir, ft_list, ft_default, options=options)
        
        if file != "":
            self.openFile (file)
            print ("Load File {}".format (file))

    def saveFile (self):
        options = QtWidgets.QFileDialog.Options ()
        ft_list, ft_default = self.getFileTypes ()
        file, used_filter = QtWidgets.QFileDialog.getSaveFileName \
            (self, QtCore.QCoreApplication.translate ("save_file", "Save File"),
             self.file_name, ft_list, ft_default, options=options)
        
        if file != "":
            self.writeFile (file)
            print ("Save File {}".format (file))

    def setFileName (self, file_name):
        config.cfg.base_dir = os.path.dirname (file_name)
        config.cfg.save ()
        self.file_name = file_name
        self.file_valid = True
        image_base_dir = os.path.join (os.path.dirname (file_name), "Images")
        self.image_base_dir = self.image_model.changeBaseDir (image_base_dir)
        self.image_file_name.setDirectory (self.image_base_dir)
        self.image_file_name.setNumber (1)
        self.image_file_name.increaseToNextNonexisting ()
        self.updateSensitivities ()

    def writeFile (self, file_name):
        data = []
        for ie in self.image_model.data_list:
            d = [ ie.file_name, ie.pos_x1, ie.pos_y1, ie.pos_x2, ie.pos_y2, ie.value ]
            data.append (d)

        if not self.file_valid:
            image_base_dir = os.path.join (os.path.dirname (file_name), "Images")
            self.image_base_dir = self.image_model.changeBaseDir (image_base_dir)

        data = { 'zz_data': data }
        data['rmse_series'] = self.rmse_series
        data['image_base_dir'] = self.image_base_dir

        f = open (file_name, "w")
        json.dump(data, f, indent=2, sort_keys=True)
        f.close ()
        
        self.setFileName (file_name)
            
    def openFile (self, filename):
        data = []
        image_base_dir = None
        self.rmse_series = []

        f = open (filename)
        s = ""
        for l in f:
            s += l
        f.close ()
        
        try:
            data = json.loads (s)
            if isinstance (data, dict):
                if 'zz_data' not in data:
                    return
                
                if 'rmse_series' in data:
                    self.rmse_series = data['rmse_series']
                
                if 'image_base_dir' in data:
                    image_base_dir = data['image_base_dir']
                
                data = data['zz_data']
            
            data2 = []
            for d in data:
                entry = None
                
                if isinstance (d, list):
                    if len (d) == 6:
                        fn, x1, y1, x2, y2, value = d
                        entry = ImageEntry (fn, value, x1, y1, x2, y2)
                    elif len (d) == 4:
                        fn, x1, y1, value = d
                        entry = ImageEntry (fn, value, x1, y1)

                if entry != None:
                    data2.append (entry)

            data = data2
            logger.info ("{} datasets loaded".format (len (data)))
            self.ui.statusbar.showMessage \
                (QtCore.QCoreApplication.translate ("status_bar",
                                                    "Loaded %n data sets from {}", "", len (data)) \
                 .format (os.path.basename (filename)), 10000)
            if image_base_dir == None:
                image_base_dir = os.path.join (os.path.dirname (filename), "Images")
                
            self.image_model.setDataList (data, image_base_dir)
            if len (data) >= 1:
                self.setFileName (filename)
                self.image_base_dir = image_base_dir
                self.selectImage (0)
                self.updateChart ()
                 
        except ValueError as e:
            logger.error ("load error: {}".format (e))
            self.image_model.setDataList ([])
            self.ui.statusbar.showMessage (QtCore.QCoreApplication.translate ("status_bar", "Failed to load data from {}") \
                                           .format (os.path.basename (filename)), 10000)

    def quitProgram (self):
        QtWidgets.qApp.exit ()

if __name__ == "__main__":
    parser = argparse.ArgumentParser (description='Dice recognition.')
    parser.add_argument ('-v', dest='verbosity', action="count", default=0,
                         help='increase verbosity')

    args = parser.parse_args()

    log_level = logging.WARNING
    if args.verbosity >= 2:
        log_level = logging.DEBUG
    elif args.verbosity == 1:
        log_level = logging.INFO

    log_handler = logging.StreamHandler ()
    log_handler.setLevel (log_level)

    logging.basicConfig (level=log_level, handlers=[])

    logger.addHandler (log_handler)
    logger.setLevel (log_level)

    app = QtWidgets.QApplication (sys.argv)

    window = MainWindow ()
    window.retranslate ()
    config.cfg.addRetranslateFunction (window.retranslate)
    window.show ()

    app.exec ()
