<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="en_US" sourcelanguage="">
<context>
    <name>config</name>
    <message>
        <location filename="config.py" line="37"/>
        <source>Base directory</source>
        <translation>Base directory</translation>
    </message>
    <message>
        <location filename="config.py" line="38"/>
        <source>Capture command</source>
        <translation>Capture command</translation>
    </message>
    <message>
        <location filename="config.py" line="39"/>
        <source>Learning rate</source>
        <translation>Learning rate</translation>
    </message>
    <message>
        <location filename="config.py" line="31"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="config.py" line="31"/>
        <source>German</source>
        <translation>German</translation>
    </message>
    <message>
        <location filename="config.py" line="40"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="config.py" line="204"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="config.py" line="208"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>error_msg</name>
    <message>
        <location filename="dice.py" line="157"/>
        <source>Invocation failed. Check the command in the preferences.</source>
        <translation>Invocation failed. Check the command in the preferences.</translation>
    </message>
    <message>
        <location filename="dice.py" line="160"/>
        <source>Process crashed. Check the command in the preferences.</source>
        <translation>Process crashed. Check the command in the preferences.</translation>
    </message>
    <message>
        <location filename="dice.py" line="163"/>
        <source>Write error</source>
        <translation>Write error</translation>
    </message>
    <message>
        <location filename="dice.py" line="166"/>
        <source>Read error</source>
        <translation>Read error</translation>
    </message>
    <message>
        <location filename="dice.py" line="187"/>
        <source>Image snap process failed</source>
        <translation>Image snap process failed</translation>
    </message>
    <message>
        <location filename="dice.py" line="203"/>
        <source>Empty image snapped</source>
        <translation>Empty image snapped</translation>
    </message>
</context>
<context>
    <name>error_title</name>
    <message>
        <location filename="dice.py" line="767"/>
        <source>Snap image failed</source>
        <translation>Snap image failed</translation>
    </message>
    <message>
        <location filename="dice.py" line="921"/>
        <source>Training error</source>
        <translation>Training error</translation>
    </message>
</context>
<context>
    <name>file_types</name>
    <message>
        <location filename="dice.py" line="1041"/>
        <source>All files</source>
        <translation>All files</translation>
    </message>
    <message>
        <location filename="dice.py" line="1042"/>
        <source>JSON files</source>
        <translation>JSON files</translation>
    </message>
</context>
<context>
    <name>image_list</name>
    <message>
        <location filename="dice.py" line="361"/>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="dice.py" line="364"/>
        <source>X1</source>
        <translation>X1</translation>
    </message>
    <message>
        <location filename="dice.py" line="367"/>
        <source>Y1</source>
        <translation>Y1</translation>
    </message>
    <message>
        <location filename="dice.py" line="370"/>
        <source>X2</source>
        <translation>X2</translation>
    </message>
    <message>
        <location filename="dice.py" line="373"/>
        <source>Y2</source>
        <translation>Y2</translation>
    </message>
    <message>
        <location filename="dice.py" line="376"/>
        <source>Value</source>
        <translation>Value</translation>
    </message>
</context>
<context>
    <name>load_file</name>
    <message>
        <location filename="dice.py" line="1051"/>
        <source>Load File</source>
        <translation>Load File</translation>
    </message>
</context>
<context>
    <name>main_window</name>
    <message>
        <location filename="main_window.ui" line="103"/>
        <source>Dice Position</source>
        <translation>Dice position</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="119"/>
        <source>Value</source>
        <translation>Value</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="138"/>
        <source>Analyze Image</source>
        <translation>Analyze Image</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="145"/>
        <source>Snap Image</source>
        <translation>Snap Image</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="162"/>
        <source>Change Data</source>
        <translation>Change Data</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="185"/>
        <source>Train</source>
        <translation>Train</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="209"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="243"/>
        <source>&amp;Save</source>
        <translation>&amp;Save</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="248"/>
        <source>&amp;Load</source>
        <translation>&amp;Load</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="253"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quit</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="250"/>
        <source>Preferences</source>
        <translation type="obsolete">Preferences</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="14"/>
        <source>Dice</source>
        <translation type="obsolete">Dice</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="220"/>
        <source>&amp;Image</source>
        <translation>&amp;Image</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="265"/>
        <source>Import Image</source>
        <translation type="obsolete">Import Image</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="270"/>
        <source>Start Training</source>
        <translation type="obsolete">Start Training</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="278"/>
        <source>&amp;Start Training</source>
        <translation>&amp;Start Training</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="283"/>
        <source>Reset Network</source>
        <translation type="obsolete">Reset Network</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="229"/>
        <source>&amp;Train</source>
        <translation>&amp;Train</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="258"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Preferences</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="263"/>
        <source>&amp;Snap Image</source>
        <translation>&amp;Snap Image</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="268"/>
        <source>&amp;Analyze Image</source>
        <translation>&amp;Analyze Image</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="273"/>
        <source>&amp;Import Image</source>
        <translation>&amp;Import Image</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="286"/>
        <source>St&amp;op Training</source>
        <translation>St&amp;op Training</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="291"/>
        <source>&amp;Reset Network</source>
        <translation>&amp;Reset Network</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="299"/>
        <source>&amp;Delete Image</source>
        <translation>&amp;Delete Image</translation>
    </message>
</context>
<context>
    <name>save_file</name>
    <message>
        <location filename="dice.py" line="1062"/>
        <source>Save File</source>
        <translation>Save File</translation>
    </message>
</context>
<context>
    <name>status_bar</name>
    <message>
        <location filename="dice.py" line="806"/>
        <source>Training active</source>
        <translation>Training active</translation>
    </message>
    <message>
        <location filename="dice.py" line="912"/>
        <source>Iteration {}, root mean squared error: {}</source>
        <translation>Iteration {}, root mean squared error: {}</translation>
    </message>
    <message>
        <location filename="dice.py" line="1161"/>
        <source>Failed to load data from {}</source>
        <translation>Failed to load data from {}</translation>
    </message>
    <message numerus="yes">
        <location filename="dice.py" line="927"/>
        <source>%n datasets loaded</source>
        <translation type="obsolete">
            <numerusform>%n dataset loaded</numerusform>
            <numerusform>%n datasets loaded</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="dice.py" line="1145"/>
        <source>Loaded %n data sets from {}</source>
        <translation>
            <numerusform>Loaded %n data set from {}</numerusform>
            <numerusform>Loaded %n data sets from {}</numerusform>
        </translation>
    </message>
    <message>
        <location filename="dice.py" line="799"/>
        <source>Analyse failed: {}</source>
        <translation>Analyse failed: {}</translation>
    </message>
    <message>
        <location filename="dice.py" line="863"/>
        <source>Training is still running</source>
        <translation>Training is still running</translation>
    </message>
    <message>
        <location filename="dice.py" line="870"/>
        <source>No training data exists</source>
        <translation>No training data exists</translation>
    </message>
    <message>
        <location filename="dice.py" line="883"/>
        <source>Training started</source>
        <translation>Training started</translation>
    </message>
    <message>
        <location filename="dice.py" line="891"/>
        <source>No training started</source>
        <translation>No training started</translation>
    </message>
    <message>
        <location filename="dice.py" line="897"/>
        <source>Abort training signaled</source>
        <translation>Abort training signaled</translation>
    </message>
</context>
</TS>
