#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

import os
import json
import sys
import collections
from PyQt5 import QtWidgets
from PyQt5 import QtCore

class ConfigEntry:
    def __init__ (self, key, default_value, cfg_name, params={}):
        self.key = key
        self.default_value = default_value
        self.value = default_value
        self.cfg_name = cfg_name
        if "visible" in params:
            self.visible = params["visible"]
            del params["visible"]
        else:
            self.visible = True
            
        self.params = params

__home_dir__ = ""
if "HOME" in os.environ:
    __home_dir__ = os.environ["HOME"]
    
__languages__ = ((QtCore.QT_TRANSLATE_NOOP ("config", "English"),
                  QtCore.QLocale.English, QtCore.QLocale.UnitedStates),
                 (QtCore.QT_TRANSLATE_NOOP ("config", "German"),
                  QtCore.QLocale.German, QtCore.QLocale.Germany))

__config_entrys__ = \
  [ ConfigEntry ("base_dir",        __home_dir__, QtCore.QT_TRANSLATE_NOOP ("config", "Base directory"), {"visible": False}),
    ConfigEntry ("capture_command", "raspistill -o -", QtCore.QT_TRANSLATE_NOOP ("config", "Capture command")),
    ConfigEntry ("learning_rate",   0.001, QtCore.QT_TRANSLATE_NOOP ("config", "Learning rate"), {"min": 1e-8, "max": 1.0, "step": 0.001, "digits": 6}),
    ConfigEntry ("language",        0, QtCore.QT_TRANSLATE_NOOP ("config", "Language"), {"combobox": [ l[0] for l in __languages__ ]})
  ]


class ConfigDB (collections.MutableMapping):
    def __init__ (self, cfg_list):
        self.d = {}
        self.l = cfg_list
        
        for i in range (len (self.l)):
            self.d[self.l[i].key] = i

    def __len__ (self):
        return self.d.__len__ ()

    def __contains__ (self, key):
        return self.d.__contains__ (key)

    def __getitem__ (self, key):
        return self.l[self.d[key]].value

    def __setitem__ (self, key, value):
        self.l[self.d[key]].value = value
    
    def __delitem__ (self, key):
        # Should not be needed
        pass
    
    def __iter__ (self):
        return self.keys ().__iter__ ()
    
    def keys (self):
        return [ e.key for e in self.l ]
    
    def getEntry (self, key):
        return self.l[self.d[key]]
    
    def getLabelName (self, key):
        return self.l[self.d[key]].cfg_name
    
    def getValueMap (self):
        d = {}
        for e in self.l:
            d[e.key] = e.value
            
        return d
 
class Config (object):
    def __init__ (self):
        self.data = ConfigDB (__config_entrys__)
        self.retranslate_functions = []
        
    def addRetranslateFunction (self, function):
        self.retranslate_functions.append (function)
        
    def getLocale (self):
        l_idx = self.language
        
        if l_idx >= len (__languages__):
            l_idx = 0

        return QtCore.QLocale (__languages__[l_idx][1], __languages__[l_idx][2])

    def getExistingLocales (self):
        return [ (l[0], QtCore.QLocale (l[1], l[2])) for l in __languages__ ]

    def __setattr__(self, *args, **kwargs):
        if len(args) >= 2:
            if args[0] != "data" and args[0] in self.data:
                self.data[args[0]] = args[1]
        
        return object.__setattr__(self, *args, **kwargs)
            
    def __getattribute__(self, *args, **kwargs):
        if len(args) >= 1:
            if args[0] != "data" and args[0] in self.data:
                return self.data[args[0]]
        
        return object.__getattribute__(self, *args, **kwargs)

    @staticmethod
    def defaultFileName ():
        dir = ""
        if "HOME" in os.environ:
            dir = os.environ["HOME"]
            
        return os.path.join (dir, ".dice.cfg")
       
    def editConfig (self, parent=None):
        d = QtWidgets.QDialog (parent)
        d.setObjectName ("edit_config_dlg")

        grid_layout_edit_config = QtWidgets.QGridLayout (d)
        #grid_layout_chart.setContentsMargins (0, 0, 0, 0)
        grid_layout_edit_config.setObjectName ("grid_layout_edit_config")
        
        row = 0
        line_edits = {}
        for k in self.data:
            if not self.data.getEntry (k).visible:
                continue
            
            lb = QtWidgets.QLabel (d)
            lb.setText (QtCore.QCoreApplication.translate ("config",
                                                           self.data.getLabelName (k)))
            grid_layout_edit_config.addWidget (lb, row, 0, 1, 1)
            
            e = None
            if "combobox" in self.data.getEntry (k).params:
                e = QtWidgets.QComboBox (d)
                for cb_entry in self.data.getEntry (k).params["combobox"]:
                    e.addItem ("{} ({})".format (QtCore.QCoreApplication.translate ("config",
                                                                                    cb_entry),
                                                 cb_entry))
                e.setCurrentIndex (self.data[k])
                grid_layout_edit_config.addWidget (e, row, 1, 1, 1)
                line_edits[row] = e
                
            elif isinstance (self.data[k], str):
                e = QtWidgets.QLineEdit (d)
                e.setText (self.data[k])
                grid_layout_edit_config.addWidget (e, row, 1, 1, 1)
                line_edits[row] = e
                
            elif isinstance (self.data[k], float):
                e = QtWidgets.QDoubleSpinBox (d)
                if "min" in self.data.getEntry (k).params:
                    e.setMinimum (self.data.getEntry (k).params["min"])

                if "max" in self.data.getEntry (k).params:
                    e.setMaximum (self.data.getEntry (k).params["max"])

                if "digits" in self.data.getEntry (k).params:
                    e.setDecimals (self.data.getEntry (k).params["digits"])

                if "step" in self.data.getEntry (k).params:
                    e.setSingleStep (self.data.getEntry (k).params["step"])

                e.setValue (self.data[k])

                grid_layout_edit_config.addWidget (e, row, 1, 1, 1)
                line_edits[row] = e
            
            if e == None:
                e = QtWidgets.QLabel (d)
                e.setText (repr (self.data[k]))
                grid_layout_edit_config.addWidget (e, row, 1, 1, 1)
                
            row += 1
            
        spacer1 = QtWidgets.QSpacerItem (5, 5, QtWidgets.QSizePolicy.Minimum,
                                         QtWidgets.QSizePolicy.Expanding)
        grid_layout_edit_config.addItem (spacer1, row, 0)
        row += 1

        button_box = QtWidgets.QHBoxLayout ()
        button_box.setObjectName ("button_box")
        
        spacer2 = QtWidgets.QSpacerItem (5, 5, QtWidgets.QSizePolicy.Expanding,
                                        QtWidgets.QSizePolicy.Minimum)
        button_box.addItem (spacer2)
        
        ok_btn = QtWidgets.QPushButton (d)
        ok_btn.setText (QtCore.QCoreApplication.translate ("config", "Ok"))
        button_box.addWidget (ok_btn)

        cancel_btn = QtWidgets.QPushButton (d)
        cancel_btn.setText (QtCore.QCoreApplication.translate ("config", "Cancel"))
        button_box.addWidget (cancel_btn)
        
        grid_layout_edit_config.addLayout (button_box, row, 0, 1, 2)
        
        ok_btn.clicked.connect (d.accept)
        cancel_btn.clicked.connect (d.reject)
        r = d.exec ()
        if r == QtWidgets.QDialog.Accepted:
            row = 0
            #line_edits = {}
            edited = False
            retranslate = False
            for k in self.data:
                if not self.data.getEntry (k).visible:
                    continue
                
                if "combobox" in self.data.getEntry (k).params:
                    v = line_edits[row].currentIndex ()
                    if v != self.data[k]:
                        self.data[k] = v
                        edited = True
                        retranslate = True
                    
                elif isinstance (self.data[k], str):
                    s = line_edits[row].text ()
                    if s != self.data[k]:
                        self.data[k] = s
                        edited = True
                
                elif isinstance (self.data[k], float):
                    v = line_edits[row].value ()
                    if v != self.data[k]:
                        self.data[k] = v
                        edited = True
                        
                row += 1
                
            if edited:
                #print ("save config to", Config.defaultFileName ())
                self.save ()

                if retranslate:
                    for fkt in self.retranslate_functions:
                        fkt ()

    def save (self, fname = None):
        if fname == None:
            fname = Config.defaultFileName ()
            
        f = open (fname, "w")
        json.dump (self.data.getValueMap (), f, indent=2, sort_keys=True)
        f.close ()
        
    def load (self, fname = None):
        if fname == None:
            fname = Config.defaultFileName ()
            
        data = None
        if os.path.isfile (fname):
            f = open (fname, "r")
            try:
                data = json.load (f)
            except Exception as e:
                print ("Error:", e)
                data = None
                
            f.close ()
            
        if not isinstance (data, dict):
            print ("failed:", repr (data))
            return
        
        for k in data:
            print ("key:", repr (k), "value:", repr (data[k]))
            if k in self.data:
                self.data[k] = data[k]


cfg = Config ()
cfg.load ()

