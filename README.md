# dice
Find out how equal balanced your dice are.

Some info in German language about this project can be found at https://lobo.mensa.uberspace.de/wp/?p=341

Take a picture of a rolled die and tell what side is up. Then activate the hardware, that rolls the die again.
Make a nice statistic about the rolled numbers.

## Dependencies

* numpy
* tensorflow
* PyQt5
* PyQtChart

## Generating needed files:

pyuic5 main_window.ui > main_window.py

pylupdate5 dice.py config.py main_window.ui -ts dice_en.ts

pylupdate5 dice.py config.py main_window.ui -ts dice_de.ts

lrelease dice_en.ts -qm dice_en.qm

lrelease dice_de.ts -qm dice_de.qm

pyrcc5 dice.qrc -o dice_rcc.py
