<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="de_DE" sourcelanguage="">
<context>
    <name>config</name>
    <message>
        <location filename="config.py" line="37"/>
        <source>Base directory</source>
        <translation>Basisverzeichnis</translation>
    </message>
    <message>
        <location filename="config.py" line="38"/>
        <source>Capture command</source>
        <translation>Aufnahmekommando</translation>
    </message>
    <message>
        <location filename="config.py" line="39"/>
        <source>Learning rate</source>
        <translation>Lernrate</translation>
    </message>
    <message>
        <location filename="config.py" line="31"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="config.py" line="31"/>
        <source>German</source>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="config.py" line="40"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="config.py" line="204"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="config.py" line="208"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>error_msg</name>
    <message>
        <location filename="dice.py" line="157"/>
        <source>Invocation failed. Check the command in the preferences.</source>
        <translation>Aufruf fehlgeschlagen. Prüfe das Kommando in den Einstellungen.</translation>
    </message>
    <message>
        <location filename="dice.py" line="160"/>
        <source>Process crashed. Check the command in the preferences.</source>
        <translation>Das Programm ist abgestürzt. Prüfe das Kommando in den Einstellungen.</translation>
    </message>
    <message>
        <location filename="dice.py" line="163"/>
        <source>Write error</source>
        <translation>Schreibfehler</translation>
    </message>
    <message>
        <location filename="dice.py" line="166"/>
        <source>Read error</source>
        <translation>Lesefehler</translation>
    </message>
    <message>
        <location filename="dice.py" line="187"/>
        <source>Image snap process failed</source>
        <translation>Aufnahmekommando fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="dice.py" line="203"/>
        <source>Empty image snapped</source>
        <translation>Leeres Bild empfangen</translation>
    </message>
</context>
<context>
    <name>error_title</name>
    <message>
        <location filename="dice.py" line="767"/>
        <source>Snap image failed</source>
        <translation>Die Bildaufnahme ist fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="dice.py" line="921"/>
        <source>Training error</source>
        <translation>Fehler beim Trainieren</translation>
    </message>
</context>
<context>
    <name>file_types</name>
    <message>
        <location filename="dice.py" line="1041"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="dice.py" line="1042"/>
        <source>JSON files</source>
        <translation>JSON-Dateien</translation>
    </message>
</context>
<context>
    <name>image_list</name>
    <message>
        <location filename="dice.py" line="361"/>
        <source>Image</source>
        <translation>Bild</translation>
    </message>
    <message>
        <location filename="dice.py" line="364"/>
        <source>X1</source>
        <translation>X1</translation>
    </message>
    <message>
        <location filename="dice.py" line="367"/>
        <source>Y1</source>
        <translation>Y1</translation>
    </message>
    <message>
        <location filename="dice.py" line="370"/>
        <source>X2</source>
        <translation>X2</translation>
    </message>
    <message>
        <location filename="dice.py" line="373"/>
        <source>Y2</source>
        <translation>Y2</translation>
    </message>
    <message>
        <location filename="dice.py" line="376"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
</context>
<context>
    <name>load_file</name>
    <message>
        <location filename="dice.py" line="1051"/>
        <source>Load File</source>
        <translation>Datei Laden</translation>
    </message>
</context>
<context>
    <name>main_window</name>
    <message>
        <location filename="main_window.ui" line="14"/>
        <source>Dice</source>
        <translation type="obsolete">Dice</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="103"/>
        <source>Dice Position</source>
        <translation>Würfelposition</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="119"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="138"/>
        <source>Analyze Image</source>
        <translation>Bild analysieren</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="145"/>
        <source>Snap Image</source>
        <translation>Bild aufnehmen</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="162"/>
        <source>Change Data</source>
        <translation>Daten übernehmen</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="185"/>
        <source>Train</source>
        <translation>Lernen</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="209"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="243"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="248"/>
        <source>&amp;Load</source>
        <translation>&amp;Laden</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="253"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="250"/>
        <source>Preferences</source>
        <translation type="obsolete">Einstellungen</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="220"/>
        <source>&amp;Image</source>
        <translation>&amp;Bild</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="265"/>
        <source>Import Image</source>
        <translation type="obsolete">Bild importieren</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="270"/>
        <source>Start Training</source>
        <translation type="obsolete">Training starten</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="278"/>
        <source>&amp;Start Training</source>
        <translation>Training &amp;starten</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="283"/>
        <source>Reset Network</source>
        <translation type="obsolete">Netzwerk zurücksetzen</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="229"/>
        <source>&amp;Train</source>
        <translation>&amp;Trainieren</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="258"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="263"/>
        <source>&amp;Snap Image</source>
        <translation>Bild &amp;aufnehmen</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="268"/>
        <source>&amp;Analyze Image</source>
        <translation>Bild a&amp;nalysieren</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="273"/>
        <source>&amp;Import Image</source>
        <translation>Bild &amp;importieren</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="286"/>
        <source>St&amp;op Training</source>
        <translation>Training &amp;beenden</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="291"/>
        <source>&amp;Reset Network</source>
        <translation>Netz &amp;zurücksetzen</translation>
    </message>
    <message>
        <location filename="main_window.ui" line="299"/>
        <source>&amp;Delete Image</source>
        <translation>Bild &amp;löschen</translation>
    </message>
</context>
<context>
    <name>save_file</name>
    <message>
        <location filename="dice.py" line="1062"/>
        <source>Save File</source>
        <translation>Datei Speichern</translation>
    </message>
</context>
<context>
    <name>status_bar</name>
    <message>
        <location filename="dice.py" line="806"/>
        <source>Training active</source>
        <translation>Training aktiv</translation>
    </message>
    <message>
        <location filename="dice.py" line="912"/>
        <source>Iteration {}, root mean squared error: {}</source>
        <translation>Iteration {}, Quadratisches Mittel der Fehler: {}</translation>
    </message>
    <message>
        <location filename="dice.py" line="1161"/>
        <source>Failed to load data from {}</source>
        <translation>Daten konnten nicht geladen werden von {}</translation>
    </message>
    <message numerus="yes">
        <location filename="dice.py" line="927"/>
        <source>%n datasets loaded</source>
        <translation type="obsolete">
            <numerusform>%n Datensatz geladen</numerusform>
            <numerusform>%n Datensätze geladen</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="dice.py" line="1145"/>
        <source>Loaded %n data sets from {}</source>
        <translation>
            <numerusform>%n Datensatz von {} geladen</numerusform>
            <numerusform>%n Datensätze von {} geladen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="dice.py" line="799"/>
        <source>Analyse failed: {}</source>
        <translation>Analyse fehlgeschlagen: {}</translation>
    </message>
    <message>
        <location filename="dice.py" line="863"/>
        <source>Training is still running</source>
        <translation>Das Training läuft noch</translation>
    </message>
    <message>
        <location filename="dice.py" line="870"/>
        <source>No training data exists</source>
        <translation>Es existieren keine Daten zum Trainieren</translation>
    </message>
    <message>
        <location filename="dice.py" line="883"/>
        <source>Training started</source>
        <translation>Training begonnen</translation>
    </message>
    <message>
        <location filename="dice.py" line="891"/>
        <source>No training started</source>
        <translation>Es ist kein Training gestartet</translation>
    </message>
    <message>
        <location filename="dice.py" line="897"/>
        <source>Abort training signaled</source>
        <translation>Abbruchssignal an den Trainingsprozess gesendet</translation>
    </message>
</context>
</TS>
